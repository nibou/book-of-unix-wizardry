# The book of Linux and Internet wizardry level 1 to 6

## Meta FAQ

### What is this ?

This is a document I decided to write to assist my godchild in learning
KungFoo. He is 17 at the time this project starts, reads english and has/wants
to learn some computer magic. I'll keep this document open so that maybe
someone else can benefit from it.

### Why am I here ? 

Maybe I saw in you the craving for "more IT magick", maybe you asked me, and I
gave you a link because I have no time. Maybe it's not me but another white
unix magician that gave you the link, who knows ... 

### Intended audience ?

You are the padawan learner, you read, you do, you try.
I write and say little, you have a lot to do to answer correctly.
There are no wrong answers, there are many incomplete answers.

This document is not a tutorial, but a "book of exercices". If followed the
author thinks you'll gain good knoledge and good practices to work in IT field.

### Who wrote this ?

I'm an IT professionnal, autodidact and working as a consultant for several
clients. Initially I fell in love with GNU, and Linux Sysadmin, but with the
years I have become a somewhat good developper too, with PHP, Python, and many
other things web-related.

Along the years in many companies, I have trained a good number of padawan
learners that gained a lot (I hope) in the process. By making this book I hope
to help more.

The list of language I coded with starts with Locomotive Basic. My first
internet used trumpet winsock. My first UNIX was SunOS. That's enough for my
resume.

### How will I learn anything ?

Exercices all the way, only exercices, no solution most of the time because
there can be many valid solutions to each exercice. Contributors are welcome to
suggest a new exercice, or the most elegant solution to an existing one.

### Licence ? 

This document and all files under this repository are under CC-BY-SA, typo
spotting, grammar fixing and general contribution is welcomed here as a git pull
request.

## Prerequisites on the way to magick.

Ok so you want to learn KungFoo ? This chapter will take you through the
prerequisite.

### Know the rules ! 

  1. This document is not the truth, does not contain all the information, and
     is a work in progress anyway. FIND the truth, LOOKUP more information, USE
     Google search, wikipedia and what nature put betweend your ears.
  1. WORK on a REAL PROJECT, personal, scholar, professionnal or whatever but
     if you only LEARN you don't KNOW. WORKING is the way to real magick. If I
     want to KNOW how to carve wood, first I LEARN, then I CHOOSE THE RIGHT
     TOOL then I WORK (on a personal project first, like a stick) THEN I know
     magic level 1. Now it's time for real woodworking, and the path to magick
     level 2.... Doing things is the key. 
  1. USE Google tools (docs/drive/mail/...) 
  1. LEARN keyboard shortcuts for everything.
  1. KNOW the jargon !! Technology is full of keywords that are borrowed from common
     language but have a very specific meaning in our fields (eg: "model").
     LEARN the words, REPEAT THEM frequently, NEVER USE anything but the
     specific word for something (eg: it's not a database, if it's a model).
  1. Learn to zoom/unzoom/branch your mind and attention quickly : look at a tree, zoom on a leaf, unzoom
     to branch, unzoom to tree, zoom back to the same leaf, repeat all while
     thinking about photosynthesis or spring flowers. Do it in real life, with
     trees, do it in conversation by asking more details and "zooming in on the
     story", classify your shopping lists like a tree of shop-aisles with leaves of
     items to buy, same for todo lists, etc... Do it everywhere to train your
     brain the ways of sorting efficiently and clustering tasks.
  1. Learn and train your brain to switch context : try to follow and
     understand two conversation at the same time (in the subway, the street,
     the mall). Try to type one text on the keyboard while listening to a song,
     or to someone talking to you. Repeat until you can alternate quickly
     enough between tasks to give the illusion that you do both at the same
     time.
  1. Be a good typographist. 

### Hardware required

 * Any PC. cheap and old is ok. 
 * If you have money focus on SSD and RAM. 
 * Internet access is a must, steal it if required.
 * A rubber duck 
 * Optional : stickers for your laptop.

### Software stack for your magician PC

Install on your PC :

  1. Latest Ubuntu stable (18.04 at time of writing). When asked, choose the
     minial install to learn how to install only what you need later.
  1. If you don't have one, create a gmail account.
  1. Create an account on bitucket or github or gitlab to host your projects
     and share work with your team. Choose the same platform that your team already
     uses.
  1. Configure Ubuntu to use your Google Account (for sync calendar, contacts ...).
  1. Install Google Chrome, use only chrome for everything.
  1. Install git and configure your name and email in global settings
  1. Install JetBrain IDE Idea. If you are a student and start from nothing,
     pick PyCharm version and learn Python. If you are a professionnal in PHP or
     other choose the Idea for your stack (eg: PHPStorm). Use Idea. Always. Buy
     it yourself if your friendly manager is a dork, it will pay itself in days
     in anger-avoidance or real cash.
  1. Learn how to use basic desktop functions (launch IDE, reboot, suspend laptop,
     workspaces, moving windows, keyboard shortcuts, lock screen...)
  1. Put rubber duck next to keyboard, looking at you.

## Audience shall stand back, now we do real magick !

Rubber duck is allowed for all exercices.

### Basic linux usage and the shell

#### Shell WTF ? 

Allowed tools : a terminal

#### FHS WTF ? 

Allowed tools : a terminal, `ls`, `cd`, `cat` and `man`.

Rubber duck asks : 

  * What is `/` ? What is `/bin` ? What is `/usr/local/bin` ? 
  * What is `mount` ? What is `man mount` ? 
  * What is `cat /etc/fstab` ? What means `man fstab` ?

#### It's broken ? It's always a disk full !

Allowed tools : a terminal

Rubber duck asks : 

  * What is `df` ? `df -h` ? 
  * What is `du` ?
  * Install package `ncdu` ? Try `ncdu /usr`

#### Install a package 

Explain to the duck what is a **package manager**. 

Tell him what is your package manager for Ubuntu Linux ? 

How does it work from the shell ? From the GUI ?

What if I want to install the `moreutils` package ?  

What does this package install on your machine ? in which folders ? 

### Play to be good

### Be a typeist 

Open a terminal, install `typespeed` package. Run it. Try to make a high score.
Try to lower your typo ratio under 15% with 3-5 words in red and KPS as high as
possible.


